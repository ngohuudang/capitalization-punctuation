from flask import Flask, request
from flask_restx import Api, Resource
import werkzeug
from werkzeug.datastructures import FileStorage
werkzeug.cached_property = werkzeug.utils.cached_property
import time
import os
import sys
if sys.platform == 'win32':
    current_dir = sys.path[0].replace('\\','/')
else:
    current_dir = os.getcwd()

sys.path.insert(1,current_dir+'/xlm-roberta-capu')
print(current_dir+'/xlm-roberta-capu')
from gec_model import GecBERTModel
cache_dir = current_dir+'/xlm-roberta-capu'
model = GecBERTModel(vocab_path=os.path.join(cache_dir, "vocabulary"), model_paths=cache_dir, split_chunk=True)

model("xin chào bạn khỏe không")[0]

# Initialize Flask app and API
app = Flask(__name__)
api = Api(app)

# Define the translation resource
capu_parser =  api.parser()
capu_parser.add_argument('text', type=str, required=True, help='Text to predict caption and punctuation')
class PredictionResource(Resource):
    @api.expect(capu_parser)
    def post(self):
        args = capu_parser.parse_args()
        try:
            # s = time.time()
            text_processed = model(args['text'])[0]
            # e = time.time()
            # print('Time executed: ', e-s)
            return {'text_processed': text_processed}, 200
        except Exception as e:
            return {'error': str(e)}, 400
        
# Add the TranslationResource to the API
api.add_resource(PredictionResource, '/predict_caption_punctuation')

if __name__ == '__main__':
    app.run(debug=False)
